package com.hzchendou.blog.demo.auth.security;

import com.hzchendou.blog.demo.common.model.vo.ResultVO;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * @Date: 2022-05-21 22:49
 * @since: 1.0
 */
@Slf4j
public class AuthAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.error("客户端信息认证失败: {}", authException.getMessage());
        response.setStatus(HttpStatus.OK.value());
        ResultVO<String> result = ResultVO.fail("认证失败");
        response.setHeader("Content-Type", "application/json;charset=utf-8");
        response.getWriter().print(result.toJson());
        response.getWriter().flush();
    }
}
