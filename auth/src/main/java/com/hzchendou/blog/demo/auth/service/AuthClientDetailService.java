package com.hzchendou.blog.demo.auth.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

/**
 * 客户端用户服务
 *
 * @Date: 2022-05-31 22:35
 * @since: 1.0
 */
@Service
public class AuthClientDetailService implements ClientDetailsService {

    private ClientDetailsService clientDetailsService;

    public AuthClientDetailService() {
        InMemoryClientDetailsServiceBuilder builder = new InMemoryClientDetailsServiceBuilder();
        builder.withClient("blog")// client_id
                .secret(new BCryptPasswordEncoder().encode("blog"))
                .resourceIds("blog", "resource")
                .authorizedGrantTypes("authorization_code", "password", "client_credentials", "implicit", "refresh_token", "sms_code")// 该client允许的授权类型 authorization_code,password,refresh_token,implicit,client_credentials
                .scopes("all", "user")// 允许的授权范围
                .autoApprove(false) //加上验证回调地址
                .authorities("blog")
                .accessTokenValiditySeconds(60 * 60 * 2) // 令牌默认有效期2小时
                .refreshTokenValiditySeconds(60 * 60 * 24 * 3) // 刷新令牌默认有效期3天
                .redirectUris("https://blog.hzchendou.com");
        try {
            this.clientDetailsService = builder.build();
        } catch (Exception e) {
            System.exit(-1);
        }
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        return this.clientDetailsService.loadClientByClientId(clientId);
    }
}
