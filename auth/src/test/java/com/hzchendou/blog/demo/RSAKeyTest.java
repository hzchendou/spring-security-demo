package com.hzchendou.blog.demo;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import org.junit.Test;

/**
 * @Date: 2022-06-07 15:07
 * @since: 1.0
 */

public class RSAKeyTest {

    @Test
    public void testKeyPair() {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(1024);
            KeyPair keyPair = keyPairGenerator.genKeyPair();
            String base64PublicKey = Base64.getEncoder()
                    .encodeToString(keyPair.getPublic().getEncoded());
            System.out.println("Base64PublicKey: " + base64PublicKey);
            String base64PrivateKey = Base64.getEncoder()
                    .encodeToString(keyPair.getPrivate().getEncoded());
            System.out.println("base64PrivateKey: " + base64PrivateKey);
        } catch (NoSuchAlgorithmException e) {
            System.exit(-1);
        }
    }

}
