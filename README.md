### SpringSecurity 入门学习
本项目包含基础项目搭建，到实战项目演练，跟着循序渐进学习可以掌握并将SpringSecurity运用到项目实战中,相关文章内容可以参考博客: [地址](https://blog.hzchendou.com?_from=gitee)
#### 所有课程涵盖内容：
- SpringSecurity 项目运行
- SpringSecurity 自定义认证流程
- SpringSecurity 自定义授权流程
- SpringSecurity OAuth2 搭建统一授权服务
- SpringSecurity OAuth2 扩展认证模式（在原先 4中认证模式基础上）
- SpringSecurity OAuth2 自定义授权流程(Gateway 严格鉴权, 资源服务器简单鉴权)
- SpringSecurity OAuth2 JWT 实现无状态 Token 认证模式
- SpringSecurity OAuth2 JWT 搭建 API 服务平台，类似微信、QQ开放平台

### 版权声明
项目可以转载使用, 转载请注明：
版权声明：本项目为时间海绵博主的原创内容，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
项目链接：https://gitee.com/hzchendou/spring-security-demo