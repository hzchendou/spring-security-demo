package com.hzchendou.blog.demo.gateway.config;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.AntPathMatcher;

/**
 * 地址白名单
 *
 * @Date: 2022-05-18 16:40
 * @since: 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "white")
public class WhiteUrlProperties {

    private static final AntPathMatcher matcher = new AntPathMatcher();
    private List<String> urls;

    public boolean containPath(String path) {
        if (urls == null || urls.size() < 0) {
            return false;
        }
        for (String url : urls) {
            if (matcher.match(url, path)) {
                return true;
            }
        }
        return false;
    }

    public String[] toUrlArray() {
        return urls == null ? new String[0] : urls.toArray(new String[0]);
    }
}
