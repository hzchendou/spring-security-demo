package com.hzchendou.blog.demo.common.enums;

/**
 * 状态码枚举类
 *
 * @Date: 2022-05-23 11:21
 * @since: 1.0
 */
public enum ResultCodeEnums {

    SUCCESS(200, "请求成功"),
    FAIL(400, "请求失败"),
    ;

    private int code;
    private String message;

    ResultCodeEnums(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
