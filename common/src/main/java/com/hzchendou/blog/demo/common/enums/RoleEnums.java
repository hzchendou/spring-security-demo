package com.hzchendou.blog.demo.common.enums;

/**
 * @Date: 2022-05-25 12:21
 * @since: 1.0
 */
public enum RoleEnums {
    /// 拒绝
    ROLE_REFUSE,
    /// 匿名访问
    ROLE_ANONYMOUS,
}
