package com.hzchendou.blog.demo.resource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * @Date: 2022-05-13 10:25
 * @since: 1.0
 */
@Configuration
public class TokenConfig {

    private String SIGNING_KEY = "hzchendou";

    /**
     * TokenStore
       *
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }
}
