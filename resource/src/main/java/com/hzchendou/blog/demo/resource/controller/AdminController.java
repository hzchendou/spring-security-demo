package com.hzchendou.blog.demo.resource.controller;

import com.hzchendou.blog.demo.common.model.vo.ResultVO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理员控制器
 *
 * @Date: 2022-05-25 12:10
 * @since: 1.0
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping("/hello")
    public ResultVO hello() {
        return ResultVO.success("Hello Admin");
    }
}
