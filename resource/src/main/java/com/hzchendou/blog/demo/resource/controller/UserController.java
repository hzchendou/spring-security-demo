package com.hzchendou.blog.demo.resource.controller;

import com.hzchendou.blog.demo.common.model.vo.ResultVO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date: 2022-05-25 12:11
 * @since: 1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/hello")
    public ResultVO hello() {
        return ResultVO.success("Hello User");
    }

    @RequestMapping("/nickname")
    public ResultVO nickname(String name) {
        return ResultVO.success("Hello " + name);
    }
}
