package com.hzchendou.blog.demo.resource.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Date: 2022-05-24 13:15
 * @since: 1.0
 */
@Data
@AllArgsConstructor
public class UserDO {

    private String username;
    private String phone;
    private List<String> roles;
}
