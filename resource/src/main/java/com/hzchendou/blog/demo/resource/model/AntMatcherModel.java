package com.hzchendou.blog.demo.resource.model;

import java.util.List;
import lombok.Data;
import org.springframework.security.access.ConfigAttribute;

/**
 * Ant-style类型匹配器
 *
 * @since: 1.0
 */
@Data
public class AntMatcherModel {

    /**
     * 匹配模式
     */
    private String antMatcherPattern;

    /**
     * 权限信息
     */
    private List<ConfigAttribute> roles;
}
