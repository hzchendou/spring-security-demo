package com.hzchendou.blog.demo.resource.service;

import com.hzchendou.blog.demo.resource.model.UserDO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * @Date: 2022-05-24 13:13
 * @since: 1.0
 */
@Service
public class PhoneService {


    /**
     * 校验手机号码
     *
     * @param phone
     * @param code
     * @return
     */
    public boolean checkPhoneCode(String phone, String code) {
        return ("15000000000".equals(phone) && "888888".equals(code)) || ("15666666666".equals(phone) && "888888".equals(code)) || "15888888888".equals(phone) && "888888".equals(code);
    }

    /**
     * 依据手机获取用户信息
     *
     * @param phone
     * @return
     */
    public UserDO queryByPhone(String phone) {
        List<String> roles = new ArrayList<>();
        if ("15888888888".equalsIgnoreCase(phone)) {
            roles.add("ROLE_ADMIN");
        }
        if ("15666666666".equalsIgnoreCase(phone)) {
            roles.add("ROLE_VIP");
        }
        roles.add("ROLE_USER");
        return new UserDO(phone, phone, roles);

    }

}
