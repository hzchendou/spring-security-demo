package com.hzchendou.blog.demo.resource.controller;

import com.hzchendou.blog.demo.common.model.vo.ResultVO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date: 2022-05-23 11:27
 * @since: 1.0
 */
@RequestMapping("/anonymity")
@RestController
public class AnonymityController {

    @RequestMapping("/hello")
    public ResultVO test() {
        return ResultVO.success("hello world");
    }

}
